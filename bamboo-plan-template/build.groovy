plan(key:'PFNODEREFSERVICE',name:'PF Node Ref Service - Build and Test Plan') {
   project(key:'FAB',name:'Product Fabric')
   repository(name:'PF Node Ref Service')
   label(name:'plan-templates')
   trigger(type:'polling',strategy:'periodically',frequency:'180') {
      repository(name:'PF Node Ref Service')
   }
   stage(name:'Test Stage',description:'Test the code to ensure that it works.') {
      job(key:'ALLTESTS',name:'All Tests',description:'Run all the tests against our code.') {
         requirement(key:'os',condition:'equals',value:'Linux')
         artifactDefinition(name:'All the things',pattern:'**/*',shared:'true')
         task(type:'checkout',description:'Checkout Default Repository') {
            repository(name:'PF Node Ref Service',checkoutDirectory:'service')
         }
         task(type:'npm',description:'install',command:'install',
            executable:'Node.js 6',workingSubDirectory:'service')
         task(type:'npm',description:'build',command:'run build',
            executable:'Node.js 6',workingSubDirectory:'service')
         task(type:'npm',description:'run tests',command:'run bamboo',
            executable:'Node.js 6',workingSubDirectory:'service')
         task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.reporter.mocha',
            description:'parse results',testPattern:'mocha.json',
            workingSubDirectory:'service')
      }
   }
   stage(name:'Build Stage',description:'Build the docker images.') {
      job(key:'DB',name:'Docker build',description:'Build docker image',
         enabled:'false') {
         task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'matches',value:'Linux')
         }
         task(type:'checkout',description:'Checkout Default Repository') {
            repository(name:'PF Node Ref Service')
         }
         task(type:'script',description:'Build image',scriptBody:'''\
        set -e
        set -o pipefail
        docker build --rm -t pf-node-ref-service .        \
''',
            workingSubDirectory:'service',interpreter:'LEGACY_SH_BAT')
      }
   }
   branchMonitoring(notificationStrategy:'INHERIT') {
      createBranch(matchingPattern:'.*')
      inactiveBranchCleanup(periodInDays:'10')
      deletedBranchCleanup(periodInDays:'1')
   }
   permissions() {
      user(name:'buildeng-bd-bot',permissions:'read,write,build,clone,administration')
      anonymous(permissions:'read')
      loggedInUser(permissions:'read')
   }
}
