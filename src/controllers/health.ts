import {iContext} from '../lib/context';

export default {
  check: (ctx: iContext) => {
    ctx.logRequest = false;
    ctx.body = 'ok';
  }
};
