/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import main from './main';
import {iContext} from '../lib/context';

describe('controllers/main', () => {
  it('should modify ctx', () => {
    let ctx:any = {};
    main.index((ctx as iContext));
    assert.equal(ctx.body, 'Product Fabric NodeJS Ref Service running :)');
  });
});
