import {ASAPHandler} from '../lib/asap';
import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';
import * as http from 'http';
import * as https from 'https';

export interface interactorOptions {
  baseUrl: string;
  authorization?: string;
  asapAudience?: string;
  timeout?: number;
  keepAlive?: boolean;
};

export class Interactor {

  /** Axios client instance */
  protected _con: AxiosInstance;

  constructor(protected opts: interactorOptions, protected asapHandler?: ASAPHandler) {
    this._con = this._createConnection();
  }

  protected _createConnection(): AxiosInstance {

    const connectionsOpts: AxiosRequestConfig = {
      baseURL: this.opts.baseUrl,
      timeout: this.opts.timeout || 1000
    };

    if (this.opts.keepAlive) {
      connectionsOpts.httpAgent = new http.Agent({ keepAlive: true });
      connectionsOpts.httpsAgent = new https.Agent({ keepAlive: true });
    }

    const connector = axios.create(connectionsOpts);
    connector.interceptors.request.use(this._requestInterceptor.bind(this));
    return connector;
  }

  protected async _requestInterceptor(requestConfig: AxiosRequestConfig) {
    return new Promise((resolve, reject) => {
      if (this.opts.authorization) {
        requestConfig.headers['Authorization'] = this.opts.authorization;
        return resolve(requestConfig);
      }

      if (this.opts.asapAudience && this.asapHandler) {
        this.asapHandler.asapHeader(this.opts.asapAudience)
          .then((bearer: string) => {
            requestConfig.headers['Authorization'] = bearer;
            resolve(requestConfig);
          })
          .catch((e) => {
            reject(e);
          });
      } else {
        reject(new Error('no authorization header or ASAP config'));
      }
    });

  }
}
