const jwtAuthentication = require('jwt-authentication');
const nodeForge = require('node-forge');

export interface ASAPKey {
  privateKey: string;
  kid: string;
}

export interface ASAPHandlerOpts {
  privateKey: string;
  issuer: string;
  audience: string;
  subject?: string;
};

const DATA_URI_PATTERN = /^data:application\/pkcs8;kid=([\w.\-\+/]+);base64,([a-zA-Z0-9+/=]+)$/;

export class ASAPHandler {

  private _keyData: ASAPKey;

  constructor(private _opts: ASAPHandlerOpts) {
    this._keyData = ASAPHandler.parseKeyUri(this._opts.privateKey);
  }

  async asapHeader(audience: string) {
    const generator = jwtAuthentication.client.create();
    const claims = {
      iss: this._opts.issuer,
      sub: this._opts.subject || this._opts.issuer,
      aud: audience
    };
    return new Promise((resolve, reject) => {
      generator.generateAuthorizationHeader(claims, this._keyData, (err: any, headerValue: string) => {
        if (err) {
          return reject(err);
        }
        resolve(headerValue);
      });
    });
  }

  static parsePrivateKey(privateKeyDerBase64: string) {
    const privateKeyDerBuffer = nodeForge.util.decode64(privateKeyDerBase64);
    const privateKeyAsn1 = nodeForge.asn1.fromDer(privateKeyDerBuffer);
    const privateKeyObj = nodeForge.pki.privateKeyFromAsn1(privateKeyAsn1);
    const privateKeyPem = nodeForge.pki.privateKeyToPem(privateKeyObj);
    return privateKeyPem.toString('base64');
  }


  static parseKeyUri(dataUri: string): ASAPKey {
    const match = DATA_URI_PATTERN.exec(decodeURIComponent(dataUri));
    if (!match) {
      throw new Error('Data URI does not match required format!');
    }

    const kid = match[1];
    const privateKey = ASAPHandler.parsePrivateKey(match[2]);

    return {
      kid,
      privateKey
    };
  }

}
