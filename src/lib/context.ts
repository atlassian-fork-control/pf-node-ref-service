import {IConfig} from '../config';
import {Logger} from 'bunyan';
import Metrics from '../lib/metrics';
import {Tracer} from '../lib/tracer';
import { iInteractorsMiddleware } from '../middleware/interactors';
import * as Router from 'koa-router';

export interface iContext extends Router.IRouterContext {
  logger: Logger;
  requestId: string;
  logRequest: boolean;
  config: IConfig;
  tracer: Tracer;
  metrics: Metrics;
  session: any;
  interactors: iInteractorsMiddleware;
}
