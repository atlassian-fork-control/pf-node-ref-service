/*globals it, describe, beforeEach, afterEach */

import {ASAPHandler, ASAPHandlerOpts} from './asap';
import * as assert from 'assert';

// Key is expired!
const keyUri = 'data:application/pkcs8;kid=test%2F88sme58d6tb1e38a;base64,MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCvLA+yxOiBi5+bStikt4bs3f3S46tPPhGBWAaqc030efkVSUcjYYvGX5GV48kRGP32/pqyseHRwjf2r4vYGBZhLRowJfp2GZiTMgGEnuA6x4VnKLzGardvuM4cr7ymSnH/F7ea3SyGz8oQ9n/KEVaG66waMz0Ic7VqzaBjjlMiGRhBnCLB5QY0eFAAyNOodIduMovoFO/Y7270uh+B/wYIDQr7fRFbikvqIL6DWO63Sw1FOpy3Y1vF+lJH75fo4t2El57XAgMUm1CtXbIyQks2Oqn95KZRt70IDYw74wHAgc2DPot+YE7D3+7O5lV1Ja5DtO1lLiS9069LBo1/yKWPAgMBAAECggEAS6MQ3DYd1uH89sboqKb+QYTRNqBLthms4Vaq+ipzfJ62KLTNXL+f5PYbHMeQaCuMbXwePA6G3VWpaIEANzyfXTJmXmfnr9r5gIVVFnirhXXlaleyu1IVBs6bYNOY94ypkGYjR2E8+rD6JTzqjMvTuspEM8PwFJgxu34yRQqNZukI7CvBJNJkfq+vVHpdnMZySdwkVUxbjUlLEWGjEPrQaS42jwnJI1i6RwGprz3b4VMKYtygvX1v6hYhvLdRa1ddJutNfvTBF3XW8EFKqLjfPCZRPFifoKH25rH0TXEpGWBSW6XROo7XGILhPNJPPuekFphvMFFOwHSNB3OKcbT60QKBgQD/j1sXmEAVxjFCtJvt9wi+VsALgmrasgvKFW6ihyjHqMaI6qhVeSRUJ3BroD8QnVXGSv6qjGxYbSfekoIFmFBIQup+Rx4rJjsu99X7v7GRUYWmMFiNZ+yekBcgFFP3OM3qKrGQDW/PqDmlccFLid61/7Ja1QFRJU5hfdu6ZSYbZQKBgQCveUXKSyOPrUtbo7HYSzZYyN1k5o3CV8BODiG1AjzyJVFB/JBEkWDec1nQbWQhPABHz4ZwZxY6qXseMVtHND/t1orox06cWTUHufqdMHmtBRkOtQOjlf1ZjgvtHCD1HvwZqWzuaSQCxxm5IMD7ppHLSLECsp62mDgtbsnKwHa/4wKBgQDDC0Q4k8iFVcgO4w0G5z1TF55mqlinJK/+wbC4dnNXPqaXpJBo+5YQbDFO0vSO16W5O092FsoyCs0c/7AgWQkcvYV+bwTFZd5RkHFOPBVnlVJtbgSHl8NtmQctQpZ63m25xG/UQ40MbZXhgRpHbqf9O3OCA4OHzPMvUQE8FyqoSQKBgF/JZJaEI9pnb86LciavD/CxXWGaFWI0Qq6ZOj80BI7uXggWVa6IaYelEi8bt+AIf+KzXyD2tQS6xZF6wegBlrA/QD7HpZrle3RHSo4KCzGuqboEiAMBenYre0ko5LjJ/DYJ7YPLczPs+Za8UXxNtiJxAOeHF+p295udcZwzKd+fAoGBALGpbc86HpaoXWsRz70IPaAlxLZ436w1Ab6yJDJpMABaWWc1OpeSSLywDMur+4NlAREcmI+v8V5DsOhvsol374gL4qODnGSXUTzGrphI4rBYd9svUrvCeet+oyYZqPMIktW9gX3a38VWsXEholpCEvGkFRDPK17eS633P6aOeI2A';
const keyBase64 = 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCvLA+yxOiBi5+bStikt4bs3f3S46tPPhGBWAaqc030efkVSUcjYYvGX5GV48kRGP32/pqyseHRwjf2r4vYGBZhLRowJfp2GZiTMgGEnuA6x4VnKLzGardvuM4cr7ymSnH/F7ea3SyGz8oQ9n/KEVaG66waMz0Ic7VqzaBjjlMiGRhBnCLB5QY0eFAAyNOodIduMovoFO/Y7270uh+B/wYIDQr7fRFbikvqIL6DWO63Sw1FOpy3Y1vF+lJH75fo4t2El57XAgMUm1CtXbIyQks2Oqn95KZRt70IDYw74wHAgc2DPot+YE7D3+7O5lV1Ja5DtO1lLiS9069LBo1/yKWPAgMBAAECggEAS6MQ3DYd1uH89sboqKb+QYTRNqBLthms4Vaq+ipzfJ62KLTNXL+f5PYbHMeQaCuMbXwePA6G3VWpaIEANzyfXTJmXmfnr9r5gIVVFnirhXXlaleyu1IVBs6bYNOY94ypkGYjR2E8+rD6JTzqjMvTuspEM8PwFJgxu34yRQqNZukI7CvBJNJkfq+vVHpdnMZySdwkVUxbjUlLEWGjEPrQaS42jwnJI1i6RwGprz3b4VMKYtygvX1v6hYhvLdRa1ddJutNfvTBF3XW8EFKqLjfPCZRPFifoKH25rH0TXEpGWBSW6XROo7XGILhPNJPPuekFphvMFFOwHSNB3OKcbT60QKBgQD/j1sXmEAVxjFCtJvt9wi+VsALgmrasgvKFW6ihyjHqMaI6qhVeSRUJ3BroD8QnVXGSv6qjGxYbSfekoIFmFBIQup+Rx4rJjsu99X7v7GRUYWmMFiNZ+yekBcgFFP3OM3qKrGQDW/PqDmlccFLid61/7Ja1QFRJU5hfdu6ZSYbZQKBgQCveUXKSyOPrUtbo7HYSzZYyN1k5o3CV8BODiG1AjzyJVFB/JBEkWDec1nQbWQhPABHz4ZwZxY6qXseMVtHND/t1orox06cWTUHufqdMHmtBRkOtQOjlf1ZjgvtHCD1HvwZqWzuaSQCxxm5IMD7ppHLSLECsp62mDgtbsnKwHa/4wKBgQDDC0Q4k8iFVcgO4w0G5z1TF55mqlinJK/+wbC4dnNXPqaXpJBo+5YQbDFO0vSO16W5O092FsoyCs0c/7AgWQkcvYV+bwTFZd5RkHFOPBVnlVJtbgSHl8NtmQctQpZ63m25xG/UQ40MbZXhgRpHbqf9O3OCA4OHzPMvUQE8FyqoSQKBgF/JZJaEI9pnb86LciavD/CxXWGaFWI0Qq6ZOj80BI7uXggWVa6IaYelEi8bt+AIf+KzXyD2tQS6xZF6wegBlrA/QD7HpZrle3RHSo4KCzGuqboEiAMBenYre0ko5LjJ/DYJ7YPLczPs+Za8UXxNtiJxAOeHF+p295udcZwzKd+fAoGBALGpbc86HpaoXWsRz70IPaAlxLZ436w1Ab6yJDJpMABaWWc1OpeSSLywDMur+4NlAREcmI+v8V5DsOhvsol374gL4qODnGSXUTzGrphI4rBYd9svUrvCeet+oyYZqPMIktW9gX3a38VWsXEholpCEvGkFRDPK17eS633P6aOeI2A';

describe('lib/asap', () => {

  describe('#parseKeyUri', () => {
    it('should throw on an invalid key uri', () => {
      assert.throws(() => {
        ASAPHandler.parseKeyUri('ass');
      }, /Error: Data URI does not match required format!/);
    });

    it('should work on valid key uri', () => {
      const keyData = ASAPHandler.parseKeyUri(keyUri);
      assert.deepEqual(keyData, {
        kid: 'test/88sme58d6tb1e38a',
        privateKey: '-----BEGIN RSA PRIVATE KEY-----\r\nMIIEpAIBAAKCAQEArywPssTogYufm0rYpLeG7N390uOrTz4RgVgGqnNN9Hn5FUlH\r\nI2GLxl+RlePJERj99v6asrHh0cI39q+L2BgWYS0aMCX6dhmYkzIBhJ7gOseFZyi8\r\nxmq3b7jOHK+8pkpx/xe3mt0shs/KEPZ/yhFWhuusGjM9CHO1as2gY45TIhkYQZwi\r\nweUGNHhQAMjTqHSHbjKL6BTv2O9u9Lofgf8GCA0K+30RW4pL6iC+g1jut0sNRTqc\r\nt2NbxfpSR++X6OLdhJee1wIDFJtQrV2yMkJLNjqp/eSmUbe9CA2MO+MBwIHNgz6L\r\nfmBOw9/uzuZVdSWuQ7TtZS4kvdOvSwaNf8iljwIDAQABAoIBAEujENw2Hdbh/PbG\r\n6Kim/kGE0TagS7YZrOFWqvoqc3yetii0zVy/n+T2GxzHkGgrjG18HjwOht1VqWiB\r\nADc8n10yZl5n56/a+YCFVRZ4q4V15WpXsrtSFQbOm2DTmPeMqZBmI0dhPPqw+iU8\r\n6ozL07rKRDPD8BSYMbt+MkUKjWbpCOwrwSTSZH6vr1R6XZzGckncJFVMW41JSxFh\r\noxD60GkuNo8JySNYukcBqa892+FTCmLcoL19b+oWIby3UWtXXSbrTX70wRd11vBB\r\nSqi43zwmUTxYn6Ch9uax9E1xKRlgUlul0TqO1xiC4TzSTz7npBaYbzBRTsB0jQdz\r\ninG0+tECgYEA/49bF5hAFcYxQrSb7fcIvlbAC4Jq2rILyhVuoocox6jGiOqoVXkk\r\nVCdwa6A/EJ1Vxkr+qoxsWG0n3pKCBZhQSELqfkceKyY7LvfV+7+xkVGFpjBYjWfs\r\nnpAXIBRT9zjN6iqxkA1vz6g5pXHBS4netf+yWtUBUSVOYX3bumUmG2UCgYEAr3lF\r\nyksjj61LW6Ox2Es2WMjdZOaNwlfATg4htQI88iVRQfyQRJFg3nNZ0G1kITwAR8+G\r\ncGcWOql7HjFbRzQ/7daK6MdOnFk1B7n6nTB5rQUZDrUDo5X9WY4L7Rwg9R78Gals\r\n7mkkAscZuSDA+6aRy0ixArKetpg4LW7JysB2v+MCgYEAwwtEOJPIhVXIDuMNBuc9\r\nUxeeZqpYpySv/sGwuHZzVz6ml6SQaPuWEGwxTtL0jteluTtPdhbKMgrNHP+wIFkJ\r\nHL2Ffm8ExWXeUZBxTjwVZ5VSbW4Eh5fDbZkHLUKWet5tucRv1EONDG2V4YEaR26n\r\n/TtzggODh8zzL1EBPBcqqEkCgYBfyWSWhCPaZ2/Oi3Imrw/wsV1hmhViNEKumTo/\r\nNASO7l4IFlWuiGmHpRIvG7fgCH/is18g9rUEusWResHoAZawP0A+x6Wa5Xt0R0qO\r\nCgsxrqm6BIgDAXp2K3tJKOS4yfw2Ce2Dy3Mz7PmWvFF8TbYicQDnhxfqdvebnXGc\r\nMynfnwKBgQCxqW3POh6WqF1rEc+9CD2gJcS2eN+sNQG+siQyaTAAWllnNTqXkki8\r\nsAzLq/uDZQERHJiPr/FeQ7Dob7KJd++IC+Kjg5xkl1E8xq6YSOKwWHfbL1K7wnnr\r\nfqMmGajzCJLVvYF92t/FVrFxIaJaQhLxpBUQzyte3kut9z+mjniNgA==\r\n-----END RSA PRIVATE KEY-----\r\n'
      });
    });
  });

  describe('#parsePrivateKey', () => {
    it('should throw on an invalid key', () => {
      assert.throws(() => {
        ASAPHandler.parsePrivateKey('ass');
      }, Error);
    });

    it('should work on valid key', () => {
      ASAPHandler.parsePrivateKey(keyBase64);
    });
  });

  describe('ASAPHandler', () => {

    it('should fail if options are not set propperly', () => {

      const opts:ASAPHandlerOpts = {
        privateKey: '',
        issuer: '',
        audience: ''
      };
      assert.throws(() => {
        new ASAPHandler(opts);
      }, /Error: Data URI does not match required format!/);
    });

    it('should work with a valid key', () => {
      const opts:ASAPHandlerOpts = {
        privateKey: keyUri,
        issuer: '',
        audience: ''
      };
      new ASAPHandler(opts);
    });

    describe('#asapHeader', () => {
      it('should return a bearer token', () => {
        const opts:ASAPHandlerOpts = {
          privateKey: keyUri,
          issuer: '',
          audience: ''
        };
        return new ASAPHandler(opts).asapHeader('testaudience')
          .then((header: any) => {
            const split = header.split(' ');
            assert.equal(split[0], 'Bearer');
            assert.ok(split[1].length > 500);
          });
      });
    });
  });
});
