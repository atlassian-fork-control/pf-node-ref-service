/*globals it, describe, beforeEach, afterEach */

import {Tracer} from './tracer';
import * as assert from 'assert';

describe('lib/tracer', () => {
  let tracer: Tracer;

  beforeEach(() => {
    tracer = new Tracer();
  });

  describe('#setName', () => {
    it('should set the traceInfo name', () => {
      tracer.setName('testname');
      assert.equal(tracer.traceInfo.name, 'testname');
    });
    it('should set the name in lowercase', () => {
      tracer.setName('UPPERCASE');
      assert.equal(tracer.traceInfo.name, 'uppercase');
    });
  });

  describe('#timingStart', () => {
    it('should set recvTimestamp', (done) => {
      const oldTS = tracer.traceInfo.recvTimestamp;
      setTimeout(() => {
        tracer.timingStart();
        assert.ok(oldTS !== tracer.traceInfo.recvTimestamp);
        done();
      }, 250);
    });

    it('should set recvTimestamp to the provided value', () => {
      tracer.timingStart(123);
      assert.equal(tracer.traceInfo.recvTimestamp, 123);
    });
  });

  describe('#timingEnd', () => {
    it('should set sendTimestamp', (done) => {
      const oldTS = tracer.traceInfo.sendTimestamp;
      setTimeout(() => {
        tracer.timingEnd();
        assert.ok(oldTS !== tracer.traceInfo.sendTimestamp);
        done();
      }, 250);
    });

    it('should set sendTimestamp to the provided value', () => {
      tracer.timingEnd(123);
      assert.equal(tracer.traceInfo.sendTimestamp, 123);
    });
  });

  describe('#readTraceInfoFromRequest', () => {
    it('builds correct traceinfo', () => {
      const requestCtx = {
        get: (header: string) => {
          switch(header) {
            case 'x-b3-traceid': return 'traceid';
            case 'x-b3-spanid': return 'spanid';
            case 'x-b3-parentspanid': return 'parentspanid';
          }
        }
      };

      tracer.readTraceInfoFromRequest((requestCtx as any));
      assert.deepEqual(tracer.traceInfo, {
        traceId: 'traceid',
        spanId: 'spanid',
        parentSpanId: 'parentspanid',
        recvTimestamp: tracer.traceInfo.recvTimestamp
      });
    });
  });

  describe('#writeTraceInfoToResponse', () => {
    it('builds correct traceinfo', () => {
      let _headers: any = {};
      const responseCtx: any = {
        set: (header: string, value: string) => _headers[header] = value
      };

      tracer.setTraceInfo({
        traceId: 'traceid',
        spanId: 'spanid'
      });

      tracer.writeTraceInfoToResponse(responseCtx);
      assert.deepEqual(_headers, {
        'x-b3-traceid': 'traceid',
        'x-b3-spanid': 'spanid'
      });

    });

    it('builds correct traceinfo incl. parent_span_id', () => {
      let _headers: any = {};
      const responseCtx: any = {
        set: (header: string, value: string) => _headers[header] = value
      };

      tracer.setTraceInfo({
        traceId: 'traceid',
        spanId: 'spanid',
        parentSpanId: 'parentspanid'
      });

      tracer.writeTraceInfoToResponse(responseCtx);
      assert.deepEqual(_headers, {
        'x-b3-traceid': 'traceid',
        'x-b3-spanid': 'spanid',
        'x-b3-parentspanid': 'parentspanid'
      });

    });
  });

  describe('#forkTraceInfo', () => {
    it('should give a child for of the current trace', () => {
      const original = tracer.traceInfo;
      const fork = tracer.forkTraceInfo();
      assert.equal(fork.traceId, original.traceId);
      assert.equal(fork.parentSpanId, original.spanId);
      assert.notEqual(fork.spanId, original.spanId);
    });
  });

  describe('#forkTracer', () => {
    it('should give a child Tracer object for of the current trace', () => {
      const original = tracer.traceInfo;
      const fork = tracer.forkTracer();
      assert.ok(fork instanceof Tracer);
      assert.equal(fork.traceInfo.traceId, original.traceId);
      assert.equal(fork.traceInfo.parentSpanId, original.spanId);
      assert.notEqual(fork.traceInfo.spanId, original.spanId);
    });
  });

  describe('#getTraceLog', () => {
    it('should return a trace log (incl. parent_span_id)', () => {
      tracer = new Tracer({
        traceId: 'traceid',
        spanId: 'spanid',
        name: 'test',
        parentSpanId: 'parentspan'
      });
      const log = tracer.getTraceLog();

      assert.ok(log.recv_timestamp);
      assert.ok(log.send_timestamp);

      delete log.recv_timestamp;
      delete log.send_timestamp;

      assert.deepEqual(log, {
        trace_id: 'traceid',
        span_id: 'spanid',
        type: 'server',
        name: 'test',
        parent_span_id: 'parentspan'
      });
    });

    it('should return a trace log (without parent_span_id)', () => {
      tracer = new Tracer({
        traceId: 'traceid',
        spanId: 'spanid',
        name: 'test',
        sendTimestamp: 123
      });
      const log = tracer.getTraceLog();

      assert.ok(log.recv_timestamp);
      delete log.recv_timestamp;

      assert.deepEqual(log, {
        trace_id: 'traceid',
        span_id: 'spanid',
        type: 'server',
        name: 'test',
        send_timestamp: 123
      });
    });
  });
});
