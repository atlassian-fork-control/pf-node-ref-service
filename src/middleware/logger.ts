import {Logger} from 'bunyan';
import {iContext} from '../lib/context';

const extend = require('deep-extend');

interface TMWLoggerOpts {
  defaultLevel?: string;
  responseTimeLimit?: number;
}

export default (logger: Logger, opts: TMWLoggerOpts = { defaultLevel: 'info' }) => {
  return async (ctx: iContext, next: Function) => {
    opts = extend(ctx.config.logger, opts);
    const startTime = new Date().getTime();
    let localLevel = opts.defaultLevel;
    ctx.logger = logger.child({
      subsystem: 'request',
      requestId: ctx.requestId
    });

    // enable request logging by default
    // controllers can disable logging via ctx
    ctx.logRequest = true;

    await next();

    const stopTime = new Date().getTime();
    const requestTime = stopTime - startTime;
    if (opts.responseTimeLimit && requestTime > opts.responseTimeLimit) {
      localLevel = 'warn';
    }

    ctx.set('X-Response-Time', requestTime + 'ms');

    const path = ctx.request.url.split('?')[0];

    ctx.tracer.setName(path);

    if (ctx.logRequest) {
      const logData = {
        request: {
          ts: startTime / 1000,
          method: ctx.method,
          url: path
        },
        response: {
          ts: stopTime / 1000,
          status: ctx.status,
          time: requestTime
        }
      };
      (ctx.logger.child({trace: ctx.tracer.getTraceLog()}) as {[key: string]: any})[localLevel](logData, '%s %s (%s) took %d ms', ctx.method, ctx.originalUrl, ctx.status, requestTime);
    }
  };
};
