
import { iContext } from '../lib/context';

export interface iInteractorsMiddleware {
}

// EXAMPLE:
/*
import { iContext } from '../lib/context';
import { ASAPHandler } from '../lib/asap';
import config from '../config';
import { AIDPlatformClient } from '../interactors/aidPlatform';
import { PresenceClient } from '../interactors/presence';

export interface iInteractorsMiddleware {
  platformClient: AIDPlatformClient;
  presenceClient: PresenceClient;
}

export default () => {
  const asapHandler = new ASAPHandler(config.asap);
  const platformClient = new AIDPlatformClient(config.platformApi, asapHandler);
  const presenceClient = new PresenceClient(config.presenceApi, asapHandler);
  return async (ctx: iContext, next: Function) => {
    ctx.interactors = {
      platformClient,
      presenceClient
    };
    await next();
  };
};
*/

export default () => {
  return async (ctx: iContext, next: Function) => {
    ctx.interactors = {};
    await next();
  };
};