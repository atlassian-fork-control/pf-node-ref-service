import Metrics from '../lib/metrics';
import { iContext } from '../lib/context';

export default (metricsInstance: Metrics) => {
  return async (ctx: iContext, next: Function) => {
    ctx.metrics = metricsInstance;
    await next();
  };
};
