import {iContext} from '../lib/context';
import {Tracer} from '../lib/tracer';

export default () => {
  return async (ctx: iContext, next: Function) => {
    ctx.tracer = new Tracer();
    ctx.tracer.readTraceInfoFromRequest(ctx);
    await next();
    ctx.tracer.writeTraceInfoToResponse(ctx);
  };
};
