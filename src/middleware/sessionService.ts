import { tSessionService } from '../config';
import { SessionValidator, tValidateResult } from '../lib/sessionValidator';
import {SessionServiceInteractor} from '../interactors/sessionService';
import {ASAPHandler} from '../lib/asap';
import {HttpError} from '../middleware/error';
import {iContext} from '../lib/context';

export default (config: tSessionService, asapHandler: ASAPHandler) => {

  const interactor = new SessionServiceInteractor(config.interactor, asapHandler);

  const validator = new SessionValidator({
    publicKeyBaseUrl: config.validator.keyserver,
    audience: config.validator.audience,
    subjects: [config.asapAudience]
  });


  return async (ctx: iContext, next: Function) => {
    const middlewareTracer = ctx.tracer.forkTracer('middleware.sessionService');
    try {
      ctx.session = null;

      const sessionToken = ctx.cookies.get(config.cookieName);
      if (sessionToken) {
        let validatorTracer = middlewareTracer.forkTracer('validate session token');
        let session = await ctx.metrics.promiseTimer('validator.validate')
          <tValidateResult>(validator.validate(sessionToken));
        ctx.logger.child({subsystem: 'tracer'}).info({trace: validatorTracer.getTraceLog()});

        if (session.timedOut) {
          const interactorTracer = middlewareTracer.forkTracer('fetch new token');
          const newToken = await ctx.metrics.promiseTimer('sessionInteractor.validateToken')
            <string>(interactor.validate(sessionToken, middlewareTracer.traceInfo));
          ctx.logger.child({subsystem: 'tracer'}).info({trace: interactorTracer.getTraceLog()});

          ctx.cookies.set(
            config.cookieName,
            newToken,
            {
              domain: config.cookieDomain,
              overwrite: true,
              secure: true,
              maxAge: 2592000000 // 30 days
            }
          );
          validatorTracer = middlewareTracer.forkTracer('validate new session token');
          session = await ctx.metrics.promiseTimer('validator.validate')
            <tValidateResult>(validator.validate(newToken));
          ctx.logger.child({subsystem: 'tracer'}).info({trace: validatorTracer.getTraceLog()});

        }
        ctx.session = session;
      }
      ctx.logger.child({subsystem: 'tracer'}).info({trace: middlewareTracer.getTraceLog()});
      // ND caching
    } catch (e) {
      ctx.logger.child({subsystem: 'tracer'}).info({trace: middlewareTracer.getTraceLog()});
      ctx.logger.child({subsystem: 'sessionservice'}).error(e);
      throw new HttpError('session invalid', {
        status: 403
      });
    }
    // ND caching

    await next();

  };
};
