/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import MWtracer from './tracer';
import {Tracer} from '../lib/tracer';
import {iContext} from '../lib/context';

let next = async () => Promise.resolve({});

describe('middleware/tracer', () => {
  it('should add a tracer to context', () => {
    let ctx:any = {
      get: () => {},
      set: () => {}
    };

    return MWtracer()((ctx as iContext), next)
      .then(() => {
        assert.ok(ctx.tracer instanceof Tracer);
      });
  });

  it('tracer should log the runtime of subsequent calls (next)', () => {
    let ctx:any = {
      get: () => {},
      set: () => {}
    };

    next = async () => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve({});
        }, 25);
      });
    };

    return MWtracer()((ctx as iContext), next)
      .then(() => {
        assert.ok(ctx.tracer instanceof Tracer);
        const log = ctx.tracer.getTraceLog();
        assert.ok(log.recv_timestamp, 'recv_timestamp');
        assert.ok(log.send_timestamp, 'send_timestamp');
        assert.ok(log.recv_timestamp < log.send_timestamp, 'ok');
      });
  });
});
