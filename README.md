# Directory Service

## Dependencies

-

## Workflow

Install the service specific dependencies

    npm install


To run the service on localhost (in a dedicated shell):

    npm run dev


Run unit tests:

    npm test


Run code coverage

    npm run cover


Generate docs

    npm run docs
